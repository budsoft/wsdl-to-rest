WSDL to RESTfull API
====================

Transform any given WSDL and expose it as a REST API,
enabling CORS GET/POST methods submitting requests and receiving responses (JSON format).

Getting Started
---------------
requires [Node.js](https://nodejs.org/dist/v4.0.0/node-v4.0.0-x64.msi) v4+ to run.
Install the dependencies and devDependencies and start the server.

```sh
$ npm install
$ PORT=8080 npm run dev

```

For production environments...

```sh
$ PORT=8080 npm start
```

**WSDL**
--------
Transform any given WSDL and expose it as a REST API.
Make direct GET/POST calls (JSON body format request and response).

* **URL**
    /api/wsdl/

* **Method:**
  `GET` | `POST`

*  **URL Params**
   **Required:**
   `url=[string]` - WSDL URL
   `endpoint=[string]` - WSDL ACTION

* **Sample Call:**

```sh
    /api/wsdl?url=[WSDL_URL]&endpoint=[WSDL_ACTION]
```

**SWAGGER YAML BUILDER**
----
Create a WSDL swagger page from any given wsdl url and service.
Will auto-redirect on-complete to SWAGGER YAML VIEWER -> /swagger-view/files/[WSDL SERVICE NAME].yaml

* **URL**
    /api/swagger-build/

* **Method:**
  `GET`

*  **URL Params**
   **Required:**
   `url=[string]` - WSDL URL
   `endpoint=[string]` - WSDL SERVICE NAME (OUTPUT -> [WSDL SERVICE NAME].yaml)

* **Sample Call:**
```sh
    /api/swagger-build?url=[WSDL_URL]&endpoint=[WSDL_SERVICE_NAME]
```

**SWAGGER YAML VIEWER**
----
View any created YAML files by the SWAGGER YAML BUILDER.
* **URL**
    /swagger-view/files/

* **Method:**
  `GET`

*  **URL Params**
   **Required:**
   `url=[string]` - WSDL SERVICE NAME

* **Sample Call:**
```sh
    /swagger-view/files?url=[HOST]/[WSDL_SERVICE_NAME].yaml
```

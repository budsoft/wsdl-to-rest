import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import pretty from 'express-prettify';
import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import config from './config.json';
//Swagger UI static path
const swaggerUiAssetPath = require("swagger-ui-dist").getAbsoluteFSPath();

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

//json
app.use(pretty({ query: 'pretty' }));

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit : config.bodyLimit
}));

// connect to db
initializeDb( db => {

	// internal middleware
	app.use(middleware({ config, db }));

	//serve files
    app.use('/files', express.static('files'));

    //init and serve Swagger UI server
    app.use('/swagger-view', express.static(swaggerUiAssetPath));

	// api router
	app.use('/api', api({ config, db }));

	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`);
	});
});

export default app;

import resource from 'resource-router-middleware';
import request from 'request';
import apicWsdl from 'apiconnect-wsdl';
import yaml from 'write-yaml';
import config from '../config.json';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'swagger',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		request();
		callback(err, id);
	},

	/** GET / - List all entities
     *
     *  Create a yml swagger API representation
     *  from WSDL web service
     *  and redirect to API UI Swagger page.
     *
     */
	index(req, res) {
        let { url, endpoint } = req.query;
        if (!url || !endpoint){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }
        let wsdlId = endpoint;
        let promise = apicWsdl.getJsonForWSDL(url);
        promise.then(function(allWSDLs) {
            let serviceName = endpoint; //Object.keys(allWSDLs[0].serviceChildren)[0];
            let wsdlEntry = apicWsdl.findWSDLForServiceName(allWSDLs, serviceName);
            if (!wsdlEntry){
                res.status(404)        // HTTP status 404: NotFound
                    .send('WSDL Service Not found: ' + serviceName);
                return console.error('WSDL Service Not found: ' + serviceName);
            }
            let swagger = apicWsdl.getSwaggerForService(wsdlEntry, serviceName, wsdlId);
            let fileName = serviceName + '.yml';
            let host = config.host;

            yaml('files/'+ fileName, swagger, function(err) {
                if (err){
                    res.send(err);
                    return console.error(err);
                }
                res.redirect(host + '/swagger-view/?url=' +host+ '/files/'+ fileName)
            });
        }, function(error) {
            res.status(404)        // HTTP status 404: NotFound
                .send(error);
            console.log(error.message);
        });
	}
});

import { version } from '../../package.json';
import { Router } from 'express';
import wsdl from './wsdl';
import swagger from './swagger';

export default ({ config, db }) => {
	let api = Router();

	// mount the wsdl resource
	api.use('/wsdl', wsdl({ config, db }));

	api.use('/swagger-build', swagger({ config, db }));

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
}

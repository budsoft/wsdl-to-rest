import resource from 'resource-router-middleware';
import wsdl from '../models/wsdl';
import request from 'request';
import soap from 'soap';
import xml2js from 'xml2js';

export default ({ config, db }) => resource({

	/** Property name to store preloaded entity on `request`. */
	id : 'wsdl',

	/** For requests with an `id`, you can auto-load the entity.
	 *  Errors terminate the request, success sets `req[id] = data`.
	 */
	load(req, id, callback) {
		request();
		callback(err, id);
	},

	/** GET / - List all entities */
	index(req, res) {
        let { url, endpoint } = req.query;
        if (!url || !endpoint){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }

        // Give the createClient Method the WSDL as the first argument
        soap.createClient(url, function(err, client){
            if (err){
                res.json(err);
                return console.error('failed:', err);
            }
            if (!client[endpoint]){
                res.json({ error: 'failed: wsdl action not found'});
                return console.error('failed: wsdl action not found');
            }
            // The Client now has all the methods of the WSDL. Use it to create a new action by feeding it the JSON Payload
            client[endpoint]((err, wsdlResult, body) => {
                if (err){
                    res.json(err);
                    return console.error('failed:', err);
                }
                xml2js.parseString(body, (err, jsonResult) => {
                    if (err){
                        res.json(err);
                        return console.error('failed:', err);
                    }
                    // Get The Result From The Soap API and Parse it to JSON
                    res.json(jsonResult);
                })
            });
        });
	},

	/** POST / - Create a new entity */
	create(req, res) {

	    let { url, endpoint } = req.query;
	    let body = req.body;

        if (!url || !endpoint || !body){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }

        // Give the createClient Method the WSDL as the first argument
        soap.createClient(url, function(err, client){
            if (err){
                res.send(err);
                return console.error('failed:', err);
            }
            if (!client[endpoint]){
                res.status(404)        // HTTP status 404: NotFound
                    .send('Not found');
                return console.error('failed: wsdl action not found');
            }
            // The Client now has all the methods of the WSDL. Use it to create a new action by feeding it the JSON Payload
            client[endpoint](body, (err, wsdlResult, wsdlBody) => {
                if (err){
                    res.send(err);
                    return console.error('failed:', err);
                }
                xml2js.parseString(wsdlBody, (err, jsonResult) => {
                    // Get The Result From The Soap API and Parse it to JSON
                    res.json(jsonResult);
                })
            });
        });
	},

	/** GET /:_wsdl_url - Return a given entity */
	read(req, res) {
        let { url, endpoint } = req.query;
        if (!url || !endpoint){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }

        // Give the createClient Method the WSDL as the first argument
        soap.createClient(url, function(err, client){
            if (err){
                res.send(res);
                return console.error('failed:', err);
            }
            if (!client[endpoint]){
                res.status(404)        // HTTP status 404: NotFound
                    .send('Not found');
                return console.error('failed: wsdl action not found');
            }
            // The Client now has all the methods of the WSDL. Use it to create a new action by feeding it the JSON Payload
            client[endpoint]((err, wsdlResult, body) => {
                if (err){
                    res.send(res);
                    return console.error('failed:', err);
                }
                xml2js.parseString(body, (err, jsonResult) => {
                    // Get The Result From The Soap API and Parse it to JSON
                    res.json(jsonResult);
                })
            });
        });
	},

	/** PUT /:_wsdl_url - Update a given entity */
	update(req, res) {
        let { url, endpoint } = req.query;
        let body = req.body;

        if (!url || !endpoint || !body){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }

        // Give the createClient Method the WSDL as the first argument
        soap.createClient(url, function(err, client){
            if (err){
                res.send(res);
                return console.error('failed:', err);
            }
            if (!client[endpoint]){
                res.status(404)        // HTTP status 404: NotFound
                    .send('Not found');
                return console.error('failed: wsdl action not found');
            }
            // The Client now has all the methods of the WSDL. Use it to create a new action by feeding it the JSON Payload
            client[endpoint](body, (err, wsdlResult, wsdlBody) => {
                if (err){
                    res.send(res);
                    return console.error('failed:', err);
                }
                xml2js.parseString(wsdlBody, (err, jsonResult) => {
                    // Get The Result From The Soap API and Parse it to JSON
                    res.json(jsonResult);
                })
            });
        });
	},

	/** DELETE - Delete a given entity */
	delete(req, res) {
        let { url, endpoint } = req.query;
        let body = req.body;

        if (!url || !endpoint || !body){
            res.status(404)        // HTTP status 404: NotFound
                .send('Not found');
            return console.error("error: missing query params.");
        }

        // Give the createClient Method the WSDL as the first argument
        soap.createClient(url, function(err, client){
            if (err){
                res.send(res);
                return console.error('failed:', err);
            }
            if (!client[endpoint]){
                res.status(404)        // HTTP status 404: NotFound
                    .send('Not found');
                return console.error('failed: wsdl action not found');
            }
            // The Client now has all the methods of the WSDL. Use it to create a new action by feeding it the JSON Payload
            client[endpoint](body, (err, wsdlResult, wsdlBody) => {
                if (err){
                    res.send(res);
                    return console.error('failed:', err);
                }
                xml2js.parseString(wsdlBody, (err, jsonResult) => {
                    // Get The Result From The Soap API and Parse it to JSON
                    res.json(jsonResult);
                })
            });
        });
	}
});
